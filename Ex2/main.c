#include <Windows.h>
#include <stdio.h>

int main(void)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi1;
	PROCESS_INFORMATION pi2;
	LPSTR process[2] = { "map.exe","write.exe" };
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi1, sizeof(pi1));
	ZeroMemory(&pi2, sizeof(pi2));
	if (!CreateProcessA(NULL, process[0], NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi1))
	{
		printf("Error!!!");
	}
	if (!CreateProcessA(NULL, process[1], NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi2))
	{
		printf("Error!!!");
	}
	WaitForSingleObject(pi1.hProcess, 30000);
	
	WaitForSingleObject(pi2.hProcess, 30000);
	CloseHandle(pi1.hProcess);
	CloseHandle(pi1.hThread);
	CloseHandle(pi2.hProcess);
	CloseHandle(pi2.hThread);
	getchar();
	return 0;
}

