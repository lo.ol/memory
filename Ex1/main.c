#include <Windows.h>
#include <stdio.h>

void countALessMemory(void);
void countA(void);
int CountChar(PCHAR pBuf, int buff_size, LPCSTR letter);

int main(void)
{
	countA();
	//countALessMemory();
	getchar();
	return 0;
}

void countA(void)
{
	HANDLE hfile = CreateFileA("gibrish.bin", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD size = GetFileSize(hfile, NULL);
	LPVOID buffer = malloc(size + 1);
	
	BOOL r = ReadFile(hfile, buffer, size, NULL, NULL);
	LPSTR data = (LPSTR)buffer;
	
	INT count = 0;
	count = CountChar(data, size, "A");
	printf("there are %d A's\n", count);
	Sleep(1000);
	free(buffer);
	CloseHandle(hfile);
}

void countALessMemory(void)
{
	HANDLE hfile = CreateFileA("gibrish.bin", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD file_size = GetFileSize(hfile, NULL);
	SYSTEM_INFO sys_info;
	GetSystemInfo(&sys_info);
	int mem_buffer_size = sys_info.dwAllocationGranularity;
	LPSTR buffer;
	
	HANDLE hMapFile = CreateFileMappingA(hfile, NULL, PAGE_READONLY, 0, 0, "file");
	INT file_location = 0, count = 0,buffer_number = 1;
	while(file_location <= (file_size - mem_buffer_size))
	{
		buffer = (LPSTR)MapViewOfFile(hMapFile,  FILE_MAP_READ,  0,  file_location, mem_buffer_size); 
		count = CountChar(buffer, mem_buffer_size, "A");
		printf("count = %d\n", count);
		buffer_number++;
		file_location = mem_buffer_size * buffer_number;
		UnmapViewOfFile(buffer);
		Sleep(100);
	}
	int reminder = file_size - file_location;
	buffer = (LPSTR)MapViewOfFile(hMapFile, FILE_MAP_READ, 0, file_location, reminder);
	count = CountChar(buffer, reminder, "A");
	printf("last count = %d", count);
	UnmapViewOfFile(buffer);
	
	CloseHandle(hMapFile);
	CloseHandle(hfile);
	Sleep(1000);
}

int CountChar(PCHAR buffer, int size, LPCSTR letter)
{ 
	static int count = 0; 
	for (INT i = 0; i < size; i++)
	{
		if (buffer[i] == *letter)
		{
			count++;
		}
	}
	return count; 
}