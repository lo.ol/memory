#include <Windows.h>
#include <stdio.h>

int main(void)
{
	HANDLE hfile = CreateFileA("gibrish.bin", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD file_size = GetFileSize(hfile, NULL);
	SYSTEM_INFO sys_info;
	GetSystemInfo(&sys_info);
	int mem_buffer_size = sys_info.dwAllocationGranularity;
	LPSTR buffer;
	HANDLE hMapFile = CreateFileMappingA(hfile, NULL, PAGE_READWRITE, 0, 0, "change");
	
	buffer = (LPSTR)MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, mem_buffer_size);
		
	Sleep(1000);
	UnmapViewOfFile(buffer);
	CloseHandle(hMapFile);
	CloseHandle(hfile);
	return 0;
}